

## Install

```bash
# go into the project
cd chat-app

# create a .env file
cp .env.example .env

# install composer dependencies
composer update

# install npm dependencies
npm install

# generate a key for your application
php artisan key:generate

# add the database connection config to your .env file
DB_CONNECTION=pgsql
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

# visit https://pusher.com and create a free app. then copy the keys into your .env file
PUSHER_APP_ID=your_pusher_app_id
PUSHER_APP_KEY=your_pusher_app_key
PUSHER_APP_SECRET=your_pusher_app_secret
PUSHER_APP_CLUSTER=your_pusher_cluster
# change the BROADCAST_DRIVER in your .env to pusher
BROADCAST_DRIVER=pusher

# run the migration files to generate the schema
php artisan migrate

# run webpack and watch for changes
npm run watch
```

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
